package me.xethh.utils.authUtils.authentication.impl.common;

import me.xethh.utils.authUtils.authentication.AccessUnit;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.ArrayList;
import java.util.Collection;

public class PureAuthUser extends User implements AccessUnit {
    public PureAuthUser(String username, String password) {
        super(username, password, new ArrayList<>());
    }

    @Override
    public String accessName() {
        return getUsername();
    }

    @Override
    public String accessToken() {
        return getPassword();
    }
}
