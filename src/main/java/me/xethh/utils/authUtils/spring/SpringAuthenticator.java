package me.xethh.utils.authUtils.spring;

import me.xethh.utils.JDBCProvider.PersistedJDBCProvider;
import me.xethh.utils.authUtils.authentication.PersistedJdbcDecisionMaker;
import me.xethh.utils.authUtils.authentication.impl.common.PureAuthUser;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class SpringAuthenticator<X extends PureAuthUser> extends PersistedJdbcDecisionMaker<X> {
    public SpringAuthenticator(PersistedJDBCProvider provider) {
        super(provider);
    }

    @Override
    public String select(Connection connection, String accessName) {
        try {
            PreparedStatement stmt = connection.prepareStatement("select password from auth where username=?");
            stmt.setString(1,accessName);
            ResultSet rs = stmt.executeQuery();
            return rs.next()?rs.getString("password"):null;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }
}
