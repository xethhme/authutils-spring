package me.xethh.utils.authUtils.spring.export;

import me.xethh.utils.JDBCProvider.JDBCProviderFactory;
import me.xethh.utils.JDBCProvider.PersistedJDBCProvider;
import me.xethh.utils.authUtils.spring.SpringAuthenticator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Import(EnableDefaultJdbcAuthenticator.Config.class)
public @interface EnableDefaultJdbcAuthenticator {
    @Configuration
    public static class Config{
        @Bean(name = "me_xethh_utils_authen_default_jdbc_provider")
        public PersistedJDBCProvider jdbcProvider(
                @Value("${me.xethh.utils.authen.default.jdbc.provider.type}") String type,
                @Value("${me.xethh.utils.authen.default.jdbc.provider.driver:NONE}") String driver,
                @Value("${me.xethh.utils.authen.default.jdbc.provider.username}") String un,
                @Value("${me.xethh.utils.authen.default.jdbc.provider.password}") String pw,
                @Value("${me.xethh.utils.authen.default.jdbc.provider.url}") String connStr
        ){
            if(type.equalsIgnoreCase("DEFAULT")){
                return JDBCProviderFactory.mysqlPersistedProvider(connStr,un,pw);
            }
            if(type.equalsIgnoreCase("CUSTOM") && driver!=null && !driver.equalsIgnoreCase("NONE")){
                return JDBCProviderFactory.createPersistedProvider(driver,connStr,un,pw);
            }
            throw new RuntimeException("Fail to create jdbc provider for spring authenticator");

        }
        @Bean(name = "me_xethh_utils_authen_default_springAuthenticator")
        public SpringAuthenticator springAuthenticator(
                @Autowired @Qualifier("me_xethh_utils_authen_default_jdbc_provider") PersistedJDBCProvider jdbcProvider
        ){
            return new SpringAuthenticator(jdbcProvider);
        }
    }
}
