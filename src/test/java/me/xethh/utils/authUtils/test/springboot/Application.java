package me.xethh.utils.authUtils.test.springboot;

import me.xethh.utils.authUtils.spring.export.EnableDefaultJdbcAuthenticator;
import org.springframework.beans.BeansException;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import java.util.Arrays;

@SpringBootApplication
@EnableDefaultJdbcAuthenticator
public class Application {
    @ComponentScan
    public static class SpringContextUtil implements ApplicationContextAware {
        private static ApplicationContext applicationContext;

        public void setApplicationContext(ApplicationContext context) throws BeansException {
            applicationContext = context;
        }

        public static ApplicationContext getApplicationContext() {
            return applicationContext;
        }
    }
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
        return args -> {

            System.out.println("Let's inspect the beans provided by Spring Boot:");

            String[] beanNames = ctx.getBeanDefinitionNames();
            Arrays.sort(beanNames);
            for (String beanName : beanNames) {
                System.out.println(beanName);
            }

        };
    }
}
