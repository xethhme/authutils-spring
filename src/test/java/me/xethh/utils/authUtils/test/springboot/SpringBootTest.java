package me.xethh.utils.authUtils.test.springboot;

import me.xethh.utils.JDBCProvider.JDBCDrivers;
import me.xethh.utils.JDBCProvider.JDBCProviderFactory;
import me.xethh.utils.JDBCProvider.PersistedJDBCProvider;
import me.xethh.utils.authUtils.authentication.Encoder;
import me.xethh.utils.authUtils.authentication.impl.common.PureAuthUser;
import me.xethh.utils.authUtils.spring.SpringAuthenticator;
import org.junit.BeforeClass;
import org.junit.Test;

import java.sql.Connection;
import java.sql.SQLException;

import static junit.framework.TestCase.assertEquals;

public class SpringBootTest {

    @BeforeClass
    public static void init() throws SQLException {
        PersistedJDBCProvider provider = JDBCProviderFactory.createPersistedProvider(JDBCDrivers.Mysql_New.driver(), "jdbc:mysql://localhost/authUtilsDB", "authUtilsDB", "authUtilsDB");

        Connection conn = provider.getConnection();
        conn.createStatement().execute("drop table if exists auth");
        conn.createStatement().execute("" +
                "create table auth(\n" +
                "\tusername varchar(64) not null,\n" +
                "    password varchar(512) not null,\n" +
                "    primary key(username)\n" +
                ")"
        );
        conn.createStatement().execute(String.format("insert into auth(username,password) values ('xeth','%s')", Encoder.newBCryptEncoder().encode("Abcd1234")));
    }
    @Test
    public void test(){
        Application.main(new String[]{});
        SpringAuthenticator springAuthen = (SpringAuthenticator) Application.SpringContextUtil.getApplicationContext().getBean("me_xethh_utils_authen_default_springAuthenticator");

        assertEquals(true,springAuthen.isValid(new PureAuthUser("xeth","Abcd1234")));
        assertEquals(false,springAuthen.isValid(new PureAuthUser("xeth","Addd")));
        assertEquals(false,springAuthen.isValid(new PureAuthUser("ddd","Abcd1234")));
    }
}
