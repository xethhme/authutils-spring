package me.xethh.utils.authUtils.test;

import me.xethh.utils.JDBCProvider.JDBCDrivers;
import me.xethh.utils.JDBCProvider.JDBCProviderFactory;
import me.xethh.utils.JDBCProvider.PersistedJDBCProvider;
import me.xethh.utils.authUtils.authentication.Encoder;
import me.xethh.utils.authUtils.authentication.PersistedJdbcDecisionMaker;
import me.xethh.utils.authUtils.authentication.impl.common.AccessUnit;
import me.xethh.utils.authUtils.authentication.impl.common.PureAuthUser;
import me.xethh.utils.authUtils.spring.SpringAuthenticator;
import org.junit.BeforeClass;
import org.junit.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static org.junit.Assert.assertEquals;

public class TestCase {
    @BeforeClass
    public static void init() throws SQLException {
        PersistedJDBCProvider provider = JDBCProviderFactory.createPersistedProvider(JDBCDrivers.Mysql_New.driver(), "jdbc:mysql://localhost/authUtilsDB", "authUtilsDB", "authUtilsDB");

        Connection conn = provider.getConnection();
        conn.createStatement().execute("drop table if exists auth");
        conn.createStatement().execute("" +
                "create table auth(\n" +
                "\tusername varchar(64) not null,\n" +
                "    password varchar(512) not null,\n" +
                "    primary key(username)\n" +
                ")"
        );
        conn.createStatement().execute(String.format("insert into auth(username,password) values ('xeth','%s')", Encoder.newBCryptEncoder().encode("Abcd1234")));
    }

    @Test
    public void test(){
        PersistedJDBCProvider provider = JDBCProviderFactory.createPersistedProvider(JDBCDrivers.Mysql_New.driver(), "jdbc:mysql://localhost/authUtilsDB", "authUtilsDB", "authUtilsDB");
        SpringAuthenticator maker = new SpringAuthenticator(provider);
        assertEquals(true,maker.isValid(new PureAuthUser("xeth","Abcd1234")));
        assertEquals(false,maker.isValid(new PureAuthUser("xeth","Addd")));
        assertEquals(false,maker.isValid(new PureAuthUser("ddd","Abcd1234")));
    }
}
